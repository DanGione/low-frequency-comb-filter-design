clear;
close all;

% read in audio file and play it back
[a,Fs] = audioread("daniel_f_voice.wav");

% plot the .wav on time axis
figure(1);
t = linspace(0, length(a)/Fs, length(a));
plot(t,a);
xlim([0, length(a)/Fs]);
% add labels
xlabel("Time (s)"); % make it look presentable
ylabel("Volts (V)");
title("Plot of Audio Voltage Signal");
movegui('east'); % move it out of the way

% run the fft and plot the magnitude of audio signal in f-domain
figure(2);
[X,f] = fft330(a,Fs);
plot(f,abs(X));
xlabel("Frequency (Hz)");    % make it pretty
ylabel("Magnitude");
title("Magnitude plot of Voice Sample");
xlim([0, 1000]); % cut off higher frequencies for visibility
movegui('west'); % move it out of the way

% play it and pause for a little
sound(a,Fs);
pause(1.5);

% define noise signal
t = 0:(1/Fs):0.5;
n = 0.8.*sin(2*pi*60*t).*exp(-40*t);
% add the harmonics for the noise
for Harm = 2:20
    n = n + (0.8/(3*pi*Harm)).*sin(2*pi*Harm*60*t).*exp(-40*t);
end

% pad it with zeros on front and back
n = [zeros(44100/2,1); n'];
n = [n; zeros((length(a)-length(n)),1)];
% add the noise to the original
x = a + n;
x = x.*0.98/max(x); % rescale everything

% plot the .wav on time axis
figure(3);
t = linspace(0, length(x)/Fs, length(x));
plot(t,x);
xlim([0, length(x)/Fs]);
% add labels
xlabel("Time (s)"); % make it look presentable
ylabel("Volts (V)");
title("Plot of Audio Voltage Signal (With Noise)");
movegui('east'); % move it out of the way

% run the fft and plot the magnitude of audio signal in f-domain
figure(4);
[X,f] = fft330(x,Fs);
plot(f,abs(X));
xlabel("Frequency (Hz)");    % make it pretty
ylabel("Magnitude");
title("Magnitude plot of Voice Sample (With Noise)");
xlim([0, 1000]); % cut off higher frequencies for visibility
movegui('west'); % move it out of the way

sound(a,Fs); % play it with noise

correctInput = false;   % create bool for input loop

while (correctInput == false)
    % tell people what to do
    fig = msgbox("Please choose the frequency you want to cut", "Notification");
    movegui(fig,'northwest');
    % get input for center frequency 
    [fc,y] = ginput(1);
    % hold the plot and add the dot
    hold on;           
    plot(fc, y, '.r');  
    hold off;    

    % tell people what to do
    fig = msgbox("Please choose the bandwidth you want to cut", "Notification");
    movegui(fig,'northwest');
    % get the bandwidth
    [bandX,bandY] = ginput(2);
    % hold plot and draw line to visualize the bandwidth
    bandwidth = abs(bandX(2)-bandX(1));
    bandX(1) = fc - bandwidth/2;
    bandX(2) = fc + bandwidth/2;
    hold on;
    plot(bandX,[y,y]);
    hold off;
    
    ans = questdlg("Is this the correct bandwidth you want cut?");
    switch ans
        case 'Yes'
            correctInput = true; 
        case 'No'
            plot(f,abs(X)); % re-plot the stuff
            xlabel("Frequency (Hz)");    % make it pretty
            ylabel("Magnitude");
            title("Magnitude plot of Voice Sample (With Noise)");
            xlim([0, 1000]); % cut off higher frequencies for visibility
        case 'Cancel'
            clc;
            close all;
            return;
    end
end

plot(f,abs(X)); % re-plot the stuff
xlabel("Frequency (Hz)");    % make it pretty
ylabel("Magnitude");
title("Magnitude plot of Voice Sample (With Noise)");
xlim([0, 1000]); % cut off higher frequencies for visibility
movegui('west'); % move it out of the way

% define the notch filter according to the user input
s = j*2*pi*f;
wo = 2*pi*fc;
H = (s.^2+wo^2)./(s.^2 + bandwidth.*s + wo^2);
% make the harmonics
for N = 2:20
    H = H.*(s.^2+(N*wo)^2)./(s.^2 + bandwidth.*s + (N*wo)^2);
end

% and apply it
figure(5);
X = X.*H;
plot(f,abs(X));
xlabel({'Frequency (Hz)'});    % make it pretty
ylabel("Magnitude");
title("Plot of Audio in Frequency Domain (Filtered)");
xlim([0, 1000]) % limit frequencies for visibility
movegui('west');

% plot the filtered voice signal
figure(6);
[x,t] = ifft330(X,Fs);
x = x.*0.98/max(x); % rescale everything
plot(t,x);
% add labels
xlabel("Time (s)"); % make it look presentable
ylabel("Volts (V)");
title("Plot of Audio Voltage Signal (Filtered)");
xlim([0, length(a)/Fs]) % make it same as the others
movegui('east');

sound(x,Fs); % play the filtered .wav
